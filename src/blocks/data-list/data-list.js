var $slick = $('.data-list__slider');

$slick.slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: false,
  infinite: false,
  swipeToSlide: true,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        variableWidth: true
      }
    },
    {
      breakpoint: 568,
      settings: 'unslick'
    }
  ]
})

$('.data-list__arrow--left').click(function () {
  $slick.slick('slickPrev')
});

$('.data-list__arrow--right').click(function () {
  $slick.slick('slickNext')
});

$slick.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
  //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
  var i = (currentSlide ? currentSlide : 0) + 1;
  $('.data-list__counter').text(i + '/' + slick.slideCount);
});
